Obrigado por estar lendo este Read-me.

Este programa foi registrado com a licensa GPL
de software aberto, para mais detalhes leia o 
arquivo GPL.txt

Nos criamos este programa para o curso
de engenharia da computação IC-UNICAMP, 2001.
Ele foi escrito completamente em assembly, sem
o uso de qualquer biblioteca externa, o codigo 
inteiro sao os dois arquivos: tetris.asm, rank.asm.
O tetris foi montado com o TASM 5 (da Borland).
Se alguem tiver interesse em modificar o codigo
para o Tasm, por favor depois nos envie o codigo adaptado!

Sinta-se a vontade para distribuir, copiar, estudar,
melhorar, etc, etc...
Qualquer modificacao interessante, ou dúvida sobre o 
programa, entre em conosco!

Obrigado,
Boa sorte,
E que a força esteja com você!
(você vai precisar... é tudo em assembly)

Autores:
José Flávio Aguilar Paulino
Gustavo Canuto

Contato:
joseflavio@gmail.com