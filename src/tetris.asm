TITLE TETRIS

; ---------------------------------------------------------------------------
; POR JOSE FLAVIO AGUILAR E GUSTAVO CANUTO, 2001
; ---------------------------------------------------------------------------

.MODEL COMPACT  ; Para usar mais de um segmento de dados
.286		; necessario para intrucao outsb/insb de words
.STACK 0100h

; ---------------------------------------------------------------------------
; FUNCOES CHAMADAS EXTERNAMENTE
; ---------------------------------------------------------------------------

PUBLIC	RankFile, Handle, PlayerName, RankData, PosScr, SScore, SNivel, MsgScrVal, MsgScrInval, MsgNome
EXTRN	RANK:near, PutPlayerScr:near, NewRank:near, ReadFile:near, CmpScr:near, MakePlace:near

; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------
; PRIMEIRO SEGMENTO DE DADOS 
; Dados gerais necessarios ao kernel do jogo
; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------

.DATA

	; ---------------------------------------------------------------------------
	; Definicoes Gerais
	; ---------------------------------------------------------------------------

	MATSIZE 	EQU	384d
	NUMCOL 	 	EQU	16d
	NUMLIN  	EQU	24d
	Velocidade_Max	EQU	3d		; Velocidade maxima do jogo, se chegar nela, zerou o jogo
	Velocidade_Ini  EQU	9d
	POSIni 		EQU	6528d		; Posicao "grafica" da matriz em bytes
	POSPreview	EQU	9670d		; Posicao "grafica" do Preview
	POSNivel	EQU	(320*130)+265 	; posicao "grafica" do nivel
	POSRank		EQU	(320*8*8)+180	; Posicao das barras de nivel no Rank

	; Caracteristicas Random
	DIVISOR		EQU	14d    		 ; Para RANDOM 100/DIVISOR, seleciona 0,1,2,3,4,5


	; Definicoes de Score e Rank
	PlayerName	DB	3 DUP (?)	; Nome do jogador para Rank
	SScore		DB	0		; Score Inicial
	SNivel		DB	1		; Nivel Inicial
	String_Vazio	DB	'  $'
	MsgScrVal	DB	'Parabens',0ah,0dh,'$'
	MsgScrInval	DB	'Perdeu, hehehe$'
	MsgNome		DB	'Digite 3 iniciais: $'
	Dentro_do_Jogo	DB	0		; Flag para marcar se esta dentro do jogo ou em menu
	Aux_Rank	DW	?		; Variavel auxiliar para desenhar o Rank

	Score_p_Passar	EQU	60d		; Score necessario para passar de nivel

	Change_Level	DB	0		; Marca se vc passou de nivel
	Arquivo_Uso	DW	?

        ; TIMER
        IntVec		DW	?,?
        Clock		DW	0
        Limite		DB	?		; Ajusta a velocidade que a peca desce
	Limite2		DB	?

	; Memoria Temporaria para Transpor
	MTmpT           DB      16 DUP (?)      
	PBUFFER         DB      16 DUP (?)
	PPreview	DB	16 DUP (?)

	;Colisao
	COLI      	EQU	7d

	; ---------------------------------------------------------------------------
	; Pecas
	; ---------------------------------------------------------------------------

	X        	EQU  9d
	Z        	EQU  8d

	PQUAD           DB      X, X, X, X
			DB      X, 1, 1, X
			DB      X, 1, 1, X
			DB      X, X, X, X

	PL              DB      X, X, X, X
			DB      X, 2, 2, 2
			DB      X, 2, X, X
			DB      X, X, X, X

	PPIPE           DB      X, X, X, X      
			DB      3, 3, 3, 3
			DB      X, X, X, X
			DB      X, X, X, X

	PT              DB      X, X, X, X
			DB      X, 4, 4, 4
			DB      X, X, 4, X
			DB      X, X, X, X

	PL2             DB      X, X, X, X
			DB      5, 5, 5, X
			DB      X, X, 5, X
			DB      X, X, X, X

	PS              DB      X, X, X, X
			DB      6, 6, X, X
			DB      X, 6, 6, X
			DB      X, X, X, X

	PS2             DB      X, X, X, X
			DB      X, 7, 7, X
			DB      7, 7, X, X
			DB      X, X, X, X


	; ---------------------------------------------------------------------------
	; Regioes de Memoria para os Pocos 
	; ---------------------------------------------------------------------------

	P2lin   DB Z, Z, Z, NUMCOL-6 DUP(0), Z, Z, Z

	PEstat  DB NUMCOL   DUP (Z)
		DB NUMLIN-4 DUP (Z,Z,Z,NUMCOL-6 DUP(0),Z,Z,Z) 
		DB 3        DUP (NUMCOL DUP (Z))
	PEstatF DB NUMCOL   DUP (Z)
		DB NUMLIN-4 DUP (Z,Z,Z,NUMCOL-6 DUP(0),Z,Z,Z) 
		DB 3        DUP (NUMCOL DUP (Z))

	PFinal  DB MATSIZE  DUP (2)      ;Poco para exibicao
	PCol    DB MATSIZE  DUP (?)      ;Poco para fazer colisao
	PVtBase DB MATSIZE  DUP (?)      ;Poco que possui a base de geracao
	PVtEsq  DB MATSIZE  DUP (?)      ;Poco possui peca deslocada esq
	PVtDir  DB MATSIZE  DUP (?)      ;Poco possui peca deslocada dir
	PVtBxo  DB MATSIZE  DUP (?)      ;Poco possui peca desl para baixo
	PVtTrp  DB MATSIZE  DUP (?)      ;Poco possui peca transposta

	; ---------------------------------------------------------------------------
	; ARQUIVOS USADOS	
	; ---------------------------------------------------------------------------

	Entrada1	db	"entrada.pic",0
	Game_Over	db	"gameover.pic",0
	Zerou		db	"zerou.pic",0
	Foto		db	"foto.pic",0

	Arq_Pausa	db	"pausa.pic",0	; Mensagem de Pausa
	Arq_Help	db	"help.pic",0	; Mensagem de Help
	Arq_Sair	db	"sair.pic",0	; Mensagem se deve Sair
	Arq_Novo	db	"novo.pic",0	; Mensagem sobre comecar novo jogo

	Level0		db	"level0.pic",0 	; 9
	Level1		db	"level1.pic",0 	; 8
	Level2		db	"level2.pic",0 	; 7
	Level3		db	"level3.pic",0	; 6
	Level4		db	"level4.pic",0	; 5
	Level5		db	"level5.pic",0	; 4
	TheRed		db	"TheRed.pic",0	; 3

	FAPart		db	"fapart.pic",0
	gusFAP		db	"gusfap.pic",0
	Animacao	db	"a0000.pic",0
			db	"a0001.pic",0
			db	"a0002.pic",0
			db	"a0003.pic",0
			db	"a0004.pic",0
			db	"a0005.pic",0
			db	"a0006.pic",0
			db	"a0007.pic",0
			db	"a0008.pic",0
			db	"a0009.pic",0
			db	"a0010.pic",0
			db	"a0011.pic",0
			db	"a0012.pic",0

	; ---------------------------------------------------------------------------
	; DEFINICOES PARA O MODO VGA
	; ---------------------------------------------------------------------------

	; Endereco de modo VGA
	VGA		EQU 	40960d ; (0a000h -> VGA video offset)

	; Paletes
	palete		db	768 DUP (63)	; Branco no resto
	palete_temp	db	768 DUP	(63)	; Palete Temporario, usado para fadein/fadeout
	palete_aux	db	768 DUP (63)	; Palete auxiliar para fazer trocas de tela sem ocorrer SNOW
	palete_preto	db	768 DUP	(0)	; Palete Preto - READ ONLY

	; Scores
	Score_Borda	db	23,23,23,23,23,23,23,23,23,23,23,23
	Score_Fill	db	23,24,24,24,24,24,24,24,24,24,24,23
	
	; Blocos
	Bloco1		db	2,2,2,2,2,2,2,4
			db	2,3,3,3,3,3,3,4
			db	2,3,3,3,3,3,3,4
			db	2,3,3,3,3,3,3,4
			db	2,3,3,3,3,3,3,4
			db	2,3,3,3,3,3,3,4
			db	2,3,3,3,3,3,3,4
			db	4,4,4,4,4,4,4,4

	Bloco2		db	5,5,5,5,5,5,5,7
			db	5,6,6,6,6,6,6,7
			db	5,6,6,6,6,6,6,7
			db	5,6,6,6,6,6,6,7
			db	5,6,6,6,6,6,6,7
			db	5,6,6,6,6,6,6,7
			db	5,6,6,6,6,6,6,7
			db	7,7,7,7,7,7,7,7

	Bloco3		db	08,08,08,08,08,08,08,10
			db	08,09,09,09,09,09,09,10
			db	08,09,09,09,09,09,09,10
			db	08,09,09,09,09,09,09,10
			db	08,09,09,09,09,09,09,10
			db	08,09,09,09,09,09,09,10
			db	08,09,09,09,09,09,09,10
			db	10,10,10,10,10,10,10,10

	Bloco4		db	11,11,11,11,11,11,11,13
			db	11,12,12,12,12,12,12,13
			db	11,12,12,12,12,12,12,13
			db	11,12,12,12,12,12,12,13
			db	11,12,12,12,12,12,12,13
			db	11,12,12,12,12,12,12,13
			db	11,12,12,12,12,12,12,13
			db	13,13,13,13,13,13,13,13

	Bloco5		db	14,14,14,14,14,14,14,16
			db	14,15,15,15,15,15,15,16
			db	14,15,15,15,15,15,15,16
			db	14,15,15,15,15,15,15,16
			db	14,15,15,15,15,15,15,16
			db	14,15,15,15,15,15,15,16
			db	14,15,15,15,15,15,15,16
			db	16,16,16,16,16,16,16,16

	Bloco6		db	17,17,17,17,17,17,17,19
			db	17,18,18,18,18,18,18,19
			db	17,18,18,18,18,18,18,19
			db	17,18,18,18,18,18,18,19
			db	17,18,18,18,18,18,18,19
			db	17,18,18,18,18,18,18,19
			db	17,18,18,18,18,18,18,19
			db	19,19,19,19,19,19,19,19

	Bloco7		db	20,20,20,20,20,20,20,22
			db	20,21,21,21,21,21,21,22
			db	20,21,21,21,21,21,21,22
			db	20,21,21,21,21,21,21,22
			db	20,21,21,21,21,21,21,22
			db	20,21,21,21,21,21,21,22
			db	20,21,21,21,21,21,21,22
			db	22,22,22,22,22,22,22,22

; ---------------------------------------------------------------------------
; Definicoes para gerar RANK de Scores
; ---------------------------------------------------------------------------

   RankFile	DB	'RANK',0
   RankData	DB	0, 5 DUP('___',0,0)	; regiao da memoria para guardar o ranking

   GoodScr	DB	?			; define se o score � valido
   PosScr	DB	0			; define posicao no score
   Handle	DW	?			; possui o file handle



; ---------------------------------------------------------------------------
; SEGUNDO SEGMENTO DE DADOS
; Buffer da Imagem de Fundo 
; ---------------------------------------------------------------------------
.fardata Image_Buffer1
	Buffer		db	64000 DUP (0)

; ---------------------------------------------------------------------------
; TERCEIRO SEGMENTO DE DADOS
; Buffer de Imagens geral
; ---------------------------------------------------------------------------
.fardata Image_Buffer2
	img		db	64000 DUP (0)

; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------
; PROGRAMA PRINCIPAL
; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------

.CODE

MAIN PROC

	MOV	AX, @DATA
	MOV	DS, AX

	; Ajusta o typematic rate do teclado
	MOV	AH, 03
	MOV	AL, 05  	; set typematic rate/delay
	MOV	BH, 0
	MOV	BL, 1Fh
	INT	16h

	; Muda o modo grafico
	MOV 	AX, 0013h   		; Seleciona o modo VGA
	INT 	10h         		; Liga o modo VGA

	; Animacao de entrada
	call	Entrada_Animacao 	; Animacao de entrada

	; --------------------------------------------------
	; Define funcao acionada pelo clock do programa
	; responsavel por descer a peca
	; --------------------------------------------------
	CALL 	BACKSET
	MOV 	DX, offset DESCONT        
	PUSH 	DS
	MOV 	AX, seg DESCONT
	MOV 	DS, AX
	CALL 	SETSET
	POP 	DS
	; --------------------------------------------------

	; --------------------------------------------------
	; Menu Principal do Jogo
	; --------------------------------------------------

	@Novo_Jogo:
	
	; Seta o flag avisando que nao esta dentro do jogo,
	; esta em menu, para rotinar de reconstrucao de tela
	mov	Dentro_do_Jogo, 0

	@Menu_Principal_Loop:
	lea	dx, Entrada1
	lea	si, palete
	mov	ax, VGA
	call	Mostra_RAW
	mov	ax, Image_Buffer1
	call	Mostra_RAW
	call 	Escreve_Palete
	
	MOV	AH, 00h
	INT	16h

	; Verifica se foi pressionado ESC
	cmp	AH, 01H	; verifica se foi pressionado ESC para sair
	jne	@Main_Segue_Novo_ESC2
	call  	MSG_Sair_Jogo
	cmp	AH, 1Fh ; verifica se o usuario apertou S para sair
	jne	@Main_Segue_Novo_ESC2
	call	Sair_do_Jogo
	@Main_Segue_Novo_ESC2:  	

	cmp	AH, 3Bh	; Verifica se foi F1 para help
	jne	@Main_Segue_Menu_Help
	call	Help_Jogo
	@Main_Segue_Menu_Help:

	cmp	AH, 3Ch	; Verifica se foi F1 para help
	je	@Main_Inicio_Rodada

	jmp @Menu_Principal_Loop
	
	; --------------------------------------------------
	; Inicio de nova rodada
	; --------------------------------------------------

@Main_Inicio_Rodada:

	mov	Limite, 255			; Pausa o clock
	call	Escurece_Palete

	; Seta flag dizendo que esta dentro de jogo
	mov	Dentro_do_Jogo, 1

	; Define Primeira imagem de fundo e variaveis
	call	Inicia_Variaveis

	; Mostra primeira imagem de fundo
	mov	dx, Arquivo_Uso
	call	Imprime_Fundo
	call	GERA_NOVA			; Gera Primeira peca e preview
	

	MOV 	DI, POSIni  			; Posicao "grafica" da matriz em bytes
			    			; Pares vao de 0-8192, linha->80 bytes

	; --------------------------------------------------------------------------------------------------
	; Loop principal do jogo
	; --------------------------------------------------------------------------------------------------

  PecaNova:  

	mov	Limite, 255			; Pausa o clock
	CALL	SCORE				; Calcula score e come linhas
	CALL	PASSEI_NIVEL
	
	; ---------------------------------------------------------
	; Verifica se Zerou o Jogo
	; ---------------------------------------------------------
	; TESTE TESTE!!!!!!!!
        cmp 	Limite2, Velocidade_Max		; verifica se zerou o jogo
	JNE	@Main_Segue_Nao_Zerou

		; Seta flag dizendo que esta ja fora de jogo
		mov	Dentro_do_Jogo, 0
		call	Show_Zerou
		call	Show_Game_Over
		call	MSG_Reiniciar_Jogo
		call	Escurece_Tela
		cmp	AH, 1Fh ; verifica se o usuario apertou S para sair
		jne	@Main_Segue_Zerou_Sair
		jmp	@Novo_Jogo
		@Main_Segue_Zerou_Sair:
		call	Sair_do_Jogo

	@Main_Segue_Nao_Zerou:
	; ---------------------------------------------------------

	CALL	GERA_NOVA			; Gera peca nova no buffer
	LEA	SI, PVtBase			; Coloca o offset do poco base em SI
	CALL	COLISAO				; Verifica se ocorreu colisao do poco em SI
	LEA	SI, PFinal			; Coloca o poco final em SI para ser desenhado
	CALL	Escreve_Tela			; Chama engine grafico responsavel por desenhar o poco na tela

	; ---------------------------------------------------------
	; Verifica se Ocorreu Game Over
	; ---------------------------------------------------------
	CMP	AH, COLI
	JNE	@Main_Segue_Nao_GameOver

		; Seta flag dizendo que esta ja fora do jogo
		mov	Dentro_do_Jogo, 0
		call	Show_Game_Over
		call	MSG_Reiniciar_Jogo
		call	Escurece_Tela

		cmp	AH, 1Fh ; verifica se o usuario apertou S para sair
		jne	@Main_Segue_Game_Over_Sair
		jmp	@Novo_Jogo
		@Main_Segue_Game_Over_Sair:
		call	Sair_do_Jogo

	@Main_Segue_Nao_GameOver:
	; ---------------------------------------------------------

  Geracao:
	CALL	MATRIXGEN
		
  Exibicao:
	MOV	DI, POSIni
	LEA	SI, PFinal
	CALL	Escreve_Tela
  
  LeTecla:     

	mov 	al, Limite2
	mov	Limite, al

	MOV	AH, 00h
	INT	16h
	MOV	AL, AH

	; ---------------------------------------------------------
	; Verifica situacoes especiais: help, pause, esc
	; ---------------------------------------------------------
	cmp	AH, 01H	; verifica se foi pressionado ESC para sair
	jne	@Main_Segue0
	call  	MSG_Sair_Jogo
	cmp	AH, 1Fh ; verifica se o usuario apertou S para sair
	jne	@Main_Segue0
	call	Sair_do_Jogo
	@Main_Segue0:  	

	cmp	AH, 3Bh	; Verifica se foi F1 para help
	jne	@Main_Segue1
	call	Help_Jogo
	@Main_Segue1:

	cmp	AH, 19h ; Verifica se P foi pressionado para pause
	jne	@Main_Segue2
	call	MSG_Pausa_Jogo
	@Main_Segue2:

	cmp	AH, 3CH	; verifica se foi pressionado F2 para reinicar
	jne	@Main_Segue3
	call  	MSG_Reiniciar_Jogo 
	cmp	AH, 1Fh ; verifica se o usuario apertou S para reiniciar
	jne	@Main_Segue3
	jmp	@Novo_Jogo	
	@Main_Segue3:  	

	

	; ---------------------------------------------------------

	CALL ANALISATECLA
	
	PUSH	AX
	CALL	COLISAO
	CMP	AH,COLI
	POP	AX  
	JNE 	Geracao
	CMP 	AL, 50h     ;analisa se o movimento tentado foi para baixo  
	JNE 	Exibicao 
	LEA 	SI, PFinal
	LEA 	BX, PEstat
	CALL 	COPIA_MATRIZ
	JMP 	PecaNova


MAIN ENDP

;-----------------------------------------------------------
; Analisa Teclas
;               AH -> possui a tecla em SCANCODE
;               SI <- endereco da regiao de memoria devido 
;                     ao movimento
;-----------------------------------------------------------
ANALISATECLA PROC
 SetaMov:      
	 CMP    AH, 50h 
	 JNE    NotDown
	 LEA    SI, PVtBxo
	 JMP    OkMov

 NotDown:
	 CMP    AH, 4Dh
	 JNE    NotRight
	 LEA    SI, PVtDir
	 JMP    OkMov

NotRight:
	 CMP    AH, 4Bh
	 JNE    NotLeft
	 LEA    SI, PVtEsq
	 JMP    OkMov

 NotLeft:
	 CMP    AH, 48h
	 JNE    NoKey
	 LEA    SI, PVtTrp
	 JMP    OkMov

   NoKey:
	 LEA    SI, PVtBase
	
   OkMov:

	RET
ANALISATECLA ENDP

; ----------------------------------------------------------
; Zera Poco, BX possui o offset do poco a zerar
; ----------------------------------------------------------
ZERA_POCO PROC

	PUSH BX
	PUSH CX

	MOV CX, MATSIZE
   PeZP:MOV byte ptr [BX], 0
	INC BX
	LOOP PeZP

	POP CX
	POP BX
	
	RET

ZERA_POCO ENDP

; ----------------------------------------------------------
; GeraPecaNova, nao espera nada
; ----------------------------------------------------------
GERA_NOVA PROC

	PUSH	BX
	PUSH	DX
	PUSH	SI

	LEA	BX, PVtBase	; Primeiro zera o poco base 
	CALL	ZERA_POCO

	LEA	SI, PVtBase	; Coloca a peca que estava no preview nele
	LEA	DX, PPreview
	MOV	BX, 6
	CALL	ESCREVEPECA_BUFFER ;coloca a peca no centro do poco

	CALL	RANDOM     ; coloca em DX o offset da peca escolhida
	CALL	Salva_Preview
	CALL	Mostra_Preview

	CALL	Desenha_Nivel
				
	POP	SI
	POP	DX
	POP	BX

	RET

GERA_NOVA ENDP

; ----------------------------------------------------------
; Random, define em dx o offset de uma das pecas diponiveis
; ----------------------------------------------------------
RANDOM PROC
	PUSH AX
	PUSH BX
	PUSH CX
	
	MOV AH,02Ch
	INT 21h

	MOV AL,DL
	XOR AH,AH
	MOV BL,DIVISOR
	DIV BL
	
	CMP AL,0d
	JNE NOTSQUARE
	LEA DX, PQUAD
	JMP RANDok

NOTSQUARE:
	  CMP AL,1d
	  JNE NOTL
	  LEA DX, PL
	  JMP RANDok
     NOTL:        
	  CMP AL,2d
	  JNE NOTL2
	  LEA DX, PPIPE
	  JMP RANDok
     NOTL2:        
	  CMP AL,3d
	  JNE NOPL2
	  LEA DX, PL2
	  JMP RANDok
     NOPL2:        
	  CMP AL,4d
	  JNE NOPS
	  LEA DX, PS
	  JMP RANDok
      NOPS:        
	  CMP AL,5d
	  JNE IST
	  LEA DX, PS2
	  JMP RANDok

      IST:
	  LEA DX, PT
   RANDok:              
	
	POP CX
	POP BX
	POP AX
	RET
RANDOM ENDP

; ----------------------------------------------------------
; Copia Matriz, MATSIZE define, SI fonte, BX destino
; ----------------------------------------------------------

COPIA_MATRIZ PROC
	PUSH AX
	PUSH CX

	MOV CX, MATSIZE
 CPLoop:
	MOV AH, [SI]
	MOV [BX], AH
	INC SI
	INC BX
	LOOP CPLoop

	POP CX
	POP AX

	RET
COPIA_MATRIZ ENDP

; ----------------------------------------------------------
; MatrixGenerator, completa as regioes com as possiveis escolhas
;        Copia PVtBase -> PVtEsq, PVtDir, PVtBxo, PVtTrp
;        Aplica funcoes
;                        TRANSPOS_PECA
;                           DESCE_PECA
;                         DIREITA_PECA
;                        ESQUERDA_PECA
; ----------------------------------------------------------
MATRIXGEN PROC
	PUSH BX
	PUSH SI

	LEA SI, PVtBase

	LEA BX, PVtEsq
	CALL COPIA_MATRIZ
	
	LEA BX, PVtDir
	CALL COPIA_MATRIZ

	LEA BX, PVtBxo
	CALL COPIA_MATRIZ

	LEA BX, PVtTrp
	CALL COPIA_MATRIZ

	LEA SI, PVtEsq
	CALL ESQUERDA_PECA

	LEA SI, PVtDir
	CALL DIREITA_PECA

	LEA SI, PVtBxo
	CALL DESCE_PECA

	LEA SI, PVtTrp
	CALL TRANSPOS_PECA

	POP SI
	POP BX

	RET
MATRIXGEN ENDP

;-----------------------------------------------------------
; Combina PocoVirtualBase (PVtBase) com PocoEstatico PEstat
; ----------------------------------------------------------
CPVTPEST PROC
	PUSH SI
	PUSH BX
	PUSH CX
	PUSH AX
	PUSH DX
	
	LEA SI, PEstat
	LEA BX, PVtBase
	LEA DX, PFinal

	MOV CX, MATSIZE
 Perc1a:
	MOV AH, [SI]    ;contem elemento de PEstat
	MOV AL, [BX]    ;contem elemento de PVtBase

	OR    AH, AL
	PUSH  SI
	MOV   SI, DX
	MOV   [SI], AH
	INC   DX
	POP   SI
	INC   SI
	INC   BX
	LOOP  Perc1a

	POP DX
	POP AX
	POP CX
	POP BX
	POP SI
	
	RET
CPVTPEST ENDP

;-----------------------------------------------------------
; Colisao, verifica se a uniao das matrizes nao gera colisao
; SI contem offset do movimento selecionado,AH recebera o 
; flag de colisao.
;-----------------------------------------------------------
COLISAO PROC
	PUSH CX
	PUSH BX
	PUSH DX
	PUSH SI

	LEA BX, PEstat
	LEA DX, PCol



	MOV CX, MATSIZE
 
 Perc1b:
	MOV AH, [SI]
	MOV AL, [BX]
	CMP AH, 0       ;elemento da selecao eh nulo
	JE SemCol2
	CMP AL, 0       ;elemento de PEstat eh nulo
	JE SemCol1
	CMP AH, X
	JE  SemCol2 
	MOV AH,COLI
	JMP FimCol


 SemCol1:
	CMP AH,X
	JNE Arruma
	XOR AH,AH

 Arruma:       
	MOV AL,AH
	JMP SemCol2
 
 SemCol2:       
	XOR AH,AH      
	PUSH SI
	MOV SI,DX
	MOV [SI],AL
	INC DX
	POP SI
	INC SI
	INC BX
	LOOP Perc1b
	
	POP SI
	LEA BX,PVtBase
	CALL COPIA_MATRIZ
	PUSH SI
	LEA DX,PCol
	MOV SI,DX
	LEA BX,PFinal
	CALL COPIA_MATRIZ  
	
 
 FimCol:
	POP SI
	POP DX
	POP BX
	POP CX

	RET
COLISAO ENDP

;-----------------------------------------------------------
; Transpoe um peca dentro de um poco de CCxLL
; ----------------------------------------------------------
TRANSPOS_PECA PROC

	; Recebe em DS:SI endereco base do poco        
	PUSH BX
	PUSH AX        
	PUSH CX
	PUSH SI

   PMAT:
	CMP byte ptr [SI], 0
	JNE FIMPMAT
	INC SI
	JMP PMAT
FIMPMAT:
	MOV BX, SI
	POP AX
	SUB BX, AX
	PUSH AX
	PUSH BX

	MOV BX, SI
	LEA DX, MTmpT   ;endereco base da peca temporaria esta em DX
	PUSH DX             
	
	MOV SI,3
	MOV CH,4
  PERC1:
	MOV CL,4
	PUSH BX
  PERC2:
	MOV AH, [BX+SI]
	PUSH BX
	MOV BX, DX
	MOV [BX], AH
	INC DX
	POP BX
	ADD BX, NUMCOL     ;pula Aij -> Atj, onde t=i+1
	DEC CL
	JNZ PERC2

	DEC SI
	POP BX
	DEC CH
	JNZ PERC1

	
	POP DX
	POP BX
	POP SI
	POP CX
	POP AX
	
	CALL ESCREVEPECA_BUFFER
	CALL PECA_TO_BUFFER
	
	POP BX
	
	RET

TRANSPOS_PECA ENDP

;-----------------------------------------------------------
;       Desce a peca no poco (DS:SI)
;-----------------------------------------------------------
DESCE_PECA PROC
	PUSH SI
	PUSH AX
	PUSH CX
	
	MOV AX, SI
	ADD AX, MATSIZE
	MOV SI, AX
	
	MOV CX, MATSIZE-NUMCOL+1
 MATESQ:
	MOV AH, [SI-NUMCOL]     ;pega o elemento para transpor
	MOV [SI], AH            ;grava o elemento na linha inferior
	DEC SI
	DEC CX
	JNZ MATESQ
	
	MOV CX, NUMCOL
  Z1LIN:                        ;zera a primeira linha
	MOV byte ptr [SI],0
	DEC SI
	DEC CX
	JNZ Z1LIN

	POP CX
	POP AX
	POP SI

	RET
DESCE_PECA ENDP

;-----------------------------------------------------------
;       Move a peca a direita no poco (DS:SI)
;-----------------------------------------------------------
DIREITA_PECA PROC

	PUSH SI
	PUSH AX
	PUSH CX
	
	MOV AX, SI
	ADD AX, MATSIZE
	MOV SI, AX
	
	MOV CX, MATSIZE
 MATESQ2:
	MOV AH, [SI-1]     ;pega o elemento para transpor
	MOV [SI], AH            ;grava o elemento na linha inferior
	DEC SI
	DEC CX
	JNZ MATESQ2

   
	MOV CX, NUMLIN
	
  Z1COL:                        ;zera a primeira coluna
	MOV byte ptr [SI],0
	ADD SI,NUMCOL
	DEC CX
	JNZ Z1COL

	POP CX
	POP AX
	POP SI

	RET
DIREITA_PECA ENDP

;-----------------------------------------------------------
;       Move a peca a esquerda no poco (DS:SI)
;-----------------------------------------------------------
ESQUERDA_PECA PROC

	PUSH SI
	PUSH AX
	PUSH CX
	
	
	
	MOV CX, MATSIZE-1
 MATESQ3:
	MOV AH, [SI+1]     ;pega o elemento para transpor
	MOV [SI], AH      ;grava o elemento na linha inferior
	INC SI
	DEC CX
	JNZ MATESQ3

   
	MOV CX, NUMLIN
  Z1COL2:                        ;zera a primeira coluna
	MOV byte ptr [SI],0
	SUB SI,NUMCOL
	DEC CX
	JNZ Z1COL2

	POP CX
	POP AX
	POP SI

	RET
ESQUERDA_PECA ENDP

;-----------------------------------------------------------
;  Copia a peca com endereco DX em PBUFFER
;-----------------------------------------------------------
PECA_TO_BUFFER PROC

	; DX registrador para endereco base da peca a ser copiada

	PUSH AX
	PUSH BX
	PUSH CX
	PUSH SI

	
	MOV BX, DX
	LEA DX, PBUFFER
	XOR SI,SI       

	MOV CX, 16
	PECA_TO_BUFFER1:

	MOV AH, [BX+SI] ; Copia mini-quad da peca para AH
	PUSH BX
	MOV BX, DX
	MOV [BX+SI], AH ; Copia o mini-quad de AX para peca-buffer
	POP BX
	INC SI

	LOOP PECA_TO_BUFFER1

	POP SI
	POP CX
	POP BX
	POP AX

	RET

PECA_TO_BUFFER ENDP

; ----------------------------------------------------------

; Escreve a peca em um dos pocos de geracao

; ----------------------------------------------------------
ESCREVEPECA_BUFFER PROC

	; SI indica endereco base da matriz
	; DX indica endereco base da peca
	; BX indica posica da peca no poco (offset)

	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX
	PUSH SI
	

	;mudando enderecamento de DS:SI par BX:SI
	MOV AX, BX ; guardo BX, futuro SI
	MOV BX, SI
	ADD BX, AX
	XOR SI, SI 
	
	MOV CL, 16
	
ESCREVEPECA_BUFFER1:

	PUSH BX
	MOV BX, DX
	MOV CH, [BX]
	INC DX          ; Caminha na matriz da peca
	POP BX          ; Tudo isso para colocar a mini-quad da peca em CH

	MOV [BX+SI], CH ; Escreve peca na matriz
	INC SI

	CMP SI, 4
	JNE ESCREVEPECA_BUFFER2
	ADD BX, NUMCOL
	XOR SI, SI
	ESCREVEPECA_BUFFER2:    
	DEC CL
	CMP CL, 0
	JNE ESCREVEPECA_BUFFER1

	POP SI
	POP DX
	POP CX
	POP BX
	POP AX
	
	RET


ESCREVEPECA_BUFFER ENDP

; ----------------------------------------------------------

ZERABUFFER PROC
	; SI endereco inicial da matriz
	
	PUSH BX
	PUSH CX
	
	MOV CX, MATSIZE + 4*NUMCOL
	XOR BX, BX
	
ZERABUFFER1:

	MOV byte ptr [BX+SI], 0
	INC BX

	LOOP ZERABUFFER1

	POP CX
	POP BX

	RET

ZERABUFFER ENDP

;---------------------------------------------------------------
; SCORE, apaga linha preenchida completamente, soma os pontos
;
;---------------------------------------------------------------

SCORE PROC
	PUSH CX
	PUSH BX
	PUSH AX  ; utilizado como acumulador       
	
	XOR AX, AX

	LEA BX, PEstat		; Esta na matriz estatica
	ADD BX, MATSIZE-1	; Voltou ao elemento anterior (ainda esta nas linhas nao visiveis)
	SUB BX, 3*NUMCOL	; Chegou ao ultimo elemento visivel da matriz
	
	XOR CX, CX    
   
 RepSc1:
	CMP byte ptr [BX], 0
	JE LinInv
	DEC BX
	INC CX
	CMP CX, NUMCOL
	JNE RepSc1
	
	INC AX          ;indica que houve uma linha completa       
	PUSH BX
	ADD BX, NUMCOL
	CALL COME_LINHA
	POP BX
	ADD BX,CX
	JMP Comi
	

 LinInv:
	ADD BX, CX
	SUB BX, NUMCOL
        CMP AX,0d
        JNE Analise
 Comi:       
	XOR CX,CX

LinInv2:
	PUSH AX
	LEA AX, PEstat
	ADD AX, numcol
	CMP BX, AX  ;estou alem do inicio da matriz
	POP AX
	JB ScoreFim
	JMP RepSc1

Analise:
	; Rotina com problema
	CALL GERA_SCORE
	XOR AX, AX
        JMP Comi

ScoreFim:

	POP AX
	POP BX
	POP CX
	
	RET  
 
SCORE ENDP

;-------------------------------------------------------------------------
; ComeLinha - come a linha e redefine a matriz
;           BX -> posicao final da linha
;-------------------------------------------------------------------------
COME_LINHA PROC
	PUSH SI
	PUSH CX
	PUSH AX
	
	; TESTE
	CALL	Pisca_Palete
	CALL 	Desenha_Nivel

	LEA SI, PEstat
	ADD SI, NUMCOL  ;inicio para atualizar
	PUSH SI        

	PUSH BX
	ADD SI, NUMCOL
	SUB BX, SI
	MOV CX, BX
	POP BX

RepCome1:
	MOV AH, [BX-NUMCOL]
	MOV [BX], AH
	DEC BX
	LOOP RepCome1

	POP SI
	LEA BX, P2Lin
	MOV CX, NUMCOL

RepCome2:		;constitui a linha mais superior, branco
	MOV AH, [BX]
	MOV [SI], AH
	INC BX
	INC SI
	LOOP RepCome2

	lea 	SI, PEstat
	call	Escreve_Tela
	POP AX
	POP CX
	POP SI
	
	RET
COME_LINHA ENDP

; ----------------------------------------------------------------------
; calcula e define score
; AX <- COMBO
; ----------------------------------------------------------------------

GERA_SCORE PROC

	pusha

	; Faz a multiplicacao de score pelo combo, combo ao quadrado
	MOV	BX, AX
     	MUL 	BL
     
     	ADD 	SScore,Al

	; Verifica se passou de nivel
	xor	cx, cx
	mov	cl, SScore	
	cmp	cl, Score_p_Passar
	jna	@GERA_SCORE_Segue1

	mov	Change_Level, 1
	mov	cl, 0
	mov	SScore, cl
	
	@GERA_SCORE_Segue1:

	popa

     	RET

GERA_SCORE ENDP

; ----------------------------------------------------------------------
; funcao que redefine uma instrucao para INT 1Ch
; ----------------------------------------------------------------------

BACKSET PROC
	;; AL=INTERRUPT NUMBER TO BE MODIFIED
	
	PUSH AX
	PUSH BX 
	PUSH ES
	
	MOV AL, 1Ch
	MOV AH,35h              ; GET VECTOR
	INT 21h
	MOV [IntVec],BX
	MOV [IntVec+2],ES
	
	POP ES
	POP BX
	POP AX
       
       
	RET
BACKSET ENDP
; ----------------------------------------------------------------------
; seta o endereco da interrupacao com o offset da nossa funcao
; DX <- offset da funcao criada
; DS <- segmento da funcao
; ----------------------------------------------------------------------

SETSET PROC        
	
	PUSH AX

	MOV AL,1Ch
	MOV AH,25h
	INT 21h
	
	
	POP AX
	
	RET     
SETSET  ENDP                    

; -----------------------------------------------------------------------
; descer a peca continuamente
; -----------------------------------------------------------------------

DESCONT PROC

	PUSH AX
	PUSH DS
	PUSH CX

	MOV AX,@DATA
	MOV DS,AX
	INC Clock
	MOV CX,Clock
	XOR AX,AX
	MOV AL,Limite
        CMP CX,AX
	JB fimdesc
	MOV AH,05h
	MOV CH,50h
	INT 16h
	MOV Clock,0


fimdesc:
	POP CX
	POP DS
	POP AX

	IRET

DESCONT ENDP

; ---------------------------------------------------------------------------
; ===========================================================================
; VGA - Rotinas para acesso ao VGA e manipulacao
; ===========================================================================
; ---------------------------------------------------------------------------

; ---------------------------------------------------------------------------
; Espera_Retrace
; ESPERA O RETRACE DA PLACA DE VIDEO
; - Nao retorna nada
; ---------------------------------------------------------------------------

Espera_Retrace PROC

	push	dx
	push	ax

	mov	dx,3dah

	@L1:
 	 in	al,dx
  	test	al,8
 	 jnz	@L1

	@L2:
	in	al,dx
 	test	al,8
 	jz	@L2

	pop	ax
	pop	dx

	ret

Espera_Retrace ENDP

; ---------------------------------------------------------------------------
; Copia_Buffer
; SUBSTITUI O BUFFER DE IMAGEM APONTADO POR DI POR SI
; SI <- Origem
; DI <- Destino
; ---------------------------------------------------------------------------

Copia_Buffer PROC

	pusha
	push	es
	push	ds

	mov	es, di
	xor	di, di

	mov	ds, si
	xor	si, si

	mov	cx, 64000

	rep	movsb

	pop	ds
	pop	es	
	popa

	ret

Copia_Buffer ENDP


; ---------------------------------------------------------------------------
; MOSTRA_RAW
; AX - buffer onde deve mostrar a imagem
; DX - offset da string com o nome do arquivo a ser lido
; SI - offset do pallete a escrever
; ---------------------------------------------------------------------------

Mostra_RAW PROC
	
	push	es
	push	ds
	pusha
	push	ax	

	; Abre um arquivo
	; cf = 0   if the file opened
	; ax = filehandle
	mov	ah, 3Dh
	mov	al, 00		; 00  read only
				; 01  write only
	     			; 02  read/write
	int	21h		; Abre o arquivo
	mov	bx, ax		; coloca o filehandle em bx

	; Le palete da Imagem

	mov	dx, si
	mov	cx, 768		; Tamanho do palete
	mov	ah, 3Fh
	int	21h

	pop	ax
	push	ax

	; Le a Imagem direto para memoria de video	
	; Define em ax -> 0a000h Segmento VGA (0a000h)
	mov	ds, ax			; Seta ES:DI para vga screen
	xor	dx, dx

	mov	cx, 64000		; Tamanho da Imagem
	mov	ah, 3Fh
	int	21h

	; Fecha um arquivo
	mov	ax, bx			; VOlta com o filehandle para ax
	mov	ah,3eh
	int	21h

	pop	ax	
	popa
	pop	ds
	pop	es

	ret	
		

Mostra_RAW ENDP


; ---------------------------------------------------------------------------
; ===========================================================================
; PALETE - Operacoes com palete
; ===========================================================================
; ---------------------------------------------------------------------------

; ---------------------------------------------------------------------------
; ESCREVE O PALETE APONTADO POR DS:SI
; ---------------------------------------------------------------------------

Escreve_Palete PROC

	push	si
	push	ax
	push	cx
	push	dx

	mov	dx, 03c8h		; Coloca em DX o valor da porta do sequenciador de palette
	xor	al, al
   	out	dx, al			; Inicia o sequenciador de palette
   	inc	dx			; Incrementa dx para a porta de dados(03c9h=Palette data port)
   	mov	cx, 768			; Ele deve escrever 768 bytes = 256 color*3 (RGB)
   	rep	outsb			; Repete "cx" vezes outsb: Output byte DS:[(E)SI] to port in DX

	pop	dx
	pop	cx
	pop	ax
	pop	si

	ret

Escreve_Palete ENDP

; ---------------------------------------------------------------------------
; Copia_Palete
;		SI <- origem
;		DI <- destino
; ---------------------------------------------------------------------------

Copia_Palete PROC

	pusha
	push	es

	mov	ax, ds
	mov	es, ax

   	mov	cx, 768			
	rep	movsb

	pop	es
	popa

	ret

Copia_Palete ENDP

; ---------------------------------------------------------------------------
; Escurece_Palete
; ---------------------------------------------------------------------------

Escurece_Palete PROC

	pusha

	lea	si, palete
	mov	cx, 62

	@Escurece_Palete2:
		push	cx
	
		mov	cx, 768			; Tamanho do Palete
		lea	di, palete_temp		; Palete Temporario

		@Escurece_Palete1:
			mov	al, [si]	; Le do palete original
			cmp	al, 2
			jl	@Escurece_Palete_Segue
			sub	al, 1		; Escurece a cor			

			@Escurece_Palete_Segue:
			mov	[di],al		; Escreve no novo palete
			inc	si
			inc 	di
			loop	@Escurece_Palete1
	
		lea	si, palete_temp

		call	Espera_Retrace
		call	Escreve_Palete

		pop	cx

		loop	@Escurece_Palete2

	popa	

	ret

Escurece_Palete ENDP

; ---------------------------------------------------------------------------
; Pisca_Palete
; ---------------------------------------------------------------------------

Pisca_Palete PROC

	pusha

	lea	si, palete
	mov	cx, 20

	@Pisca_Palete2:
		push	cx
	
		mov	cx, 768			; Tamanho do Palete
		lea	di, palete_temp		; Palete Temporario

		@Pisca_Palete1:
			mov	al, [si]	; Le do palete original
			cmp	al, 60
			jg	@Pisca_Palete_Segue
			cmp	cx, 693
			jg	@Pisca_Palete_Segue
			add	al, 3		; Escurece a cor			

			@Pisca_Palete_Segue:
			mov	[di],al		; Escreve no novo palete
			inc	si
			inc 	di
			loop	@Pisca_Palete1

		lea	si, palete_temp

		call	Espera_Retrace
		call	Escreve_Palete

		pop	cx

		loop	@Pisca_Palete2

	; Para Voltar com o palete original
	lea	si, palete

	call	Escreve_Palete

	popa	

	ret

Pisca_Palete ENDP

; ---------------------------------------------------------------------------
; Pisca_Palete2 - Pisca o palete TODO
; ---------------------------------------------------------------------------

Pisca_Palete2 PROC

	pusha

	lea	si, palete
	mov	cx, 20

	@Pisca_Palete22:
		push	cx
	
		mov	cx, 768			; Tamanho do Palete
		lea	di, palete_temp		; Palete Temporario

		@Pisca_Palete21:
			mov	al, [si]	; Le do palete original
			cmp	al, 60
			jg	@Pisca_Palete_Segue2
			add	al, 3		; Escurece a cor			
			@Pisca_Palete_Segue2:

			mov	[di],al		; Escreve no novo palete
			inc	si
			inc 	di
			loop	@Pisca_Palete21

		lea	si, palete_temp

		call	Espera_Retrace
		call	Espera_Retrace
		call	Escreve_Palete

		pop	cx

		loop	@Pisca_Palete22

	; Para Voltar com o palete original

	lea	si, palete
	call	Espera_Retrace
	call	Escreve_Palete

	popa	

	ret

Pisca_Palete2 ENDP

; ---------------------------------------------------------------------------
; Escurece_Tela - Escure a tela bruscamente para pausa, nao retorna nada
; ---------------------------------------------------------------------------

Escurece_Tela PROC

	pusha

	lea	si, palete
	mov	cx, 17				; Passos para escurecer

	@Escurece_Tela1:
		push	cx
	
		mov	cx, 768			; Tamanho do Palete - 2 cores
		lea	di, palete_temp		; Palete Temporario

		@Escurece_Tela2:

			mov	al, [si]	; Le do palete original

			cmp	cx, 762
			ja	@Escurece_Tela_Segue
			cmp	al, 3
			jl	@Escurece_Tela_Segue
			sub	al, 2		; Escurece a cor
			
			@Escurece_Tela_Segue:
			mov	[di], al	; Escreve no novo palete
			inc	si
			inc 	di
			loop	@Escurece_Tela2
	
		lea	si, palete_temp

		call	Espera_Retrace
		call	Escreve_Palete

		pop	cx

		loop	@Escurece_Tela1

	popa	

	ret

Escurece_Tela ENDP

; ---------------------------------------------------------------------------
; Salva_Preview
; offset da peca em DX
; Peca sempre copiada para PPreview
; ---------------------------------------------------------------------------

Salva_Preview PROC

	pusha

	mov	si, dx

	lea	di, PPreview
	mov	cx, 16

	@Salva_Preview1:

	mov	al, [si]
	mov	[di],al
	inc	si
	inc	di
	
	loop	@Salva_Preview1

	popa

	ret

Salva_Preview ENDP

; ---------------------------------------------------------------------------
; ===========================================================================
; ENGINE - Funcoes para engine grafico do jogo
; ===========================================================================
; ---------------------------------------------------------------------------

; ---------------------------------------------------------------------------
; Escreve_Tela
; ---------------------------------------------------------------------------

Escreve_Tela PROC

	push	ax
	push	es
	push	di
	push	si
	push	cx
	push	bx

	mov	ax, 0a000h		; Segmento VGA (0a000h)
	mov	es, ax			; Seta ES:DI para vga screen
	mov	bx, si			; Coloca em bx o Offset da matriz a ser desenhada

	add	bx, NUMCOL		; Salta Primeira Linha

	mov	cx, (NUMLIN-4)*8
	xor	ax, ax
	@Escreve_Tela_Coluna:

		; Guarda a posicao de em qual linha esta na matriz
		push	cx
		push	bx	
		
		; Pula as linhas e colunas invisiveis
		mov	cx, (NUMCOL-6)
		add	bx, 3

		@Escreve_Tela_Linha:
						
			cmp	byte ptr ds:[bx],0
			je	@Nao_Bloco		
				call	Escolhe_Bloco
				jmp	@Desenha
			
			; ----------------------------------------------------------
			; Nao Desenha um Bloco, substitui por background
			; ----------------------------------------------------------
			@Nao_Bloco:
			
				push	ds
				push	si
				
				mov	si, Image_Buffer1
				mov	ds, si
				mov	si, di
				
				movsw	
				movsw	
				movsw	
				movsw	

				pop	si
				pop	ds

				
				jmp     @Fim_do_Desenho
				
			; ----------------------------------------------------------
			; Desenha um Bloco do tipo definido pelo offset de SI
			; ----------------------------------------------------------
			@Desenha:			
					
				; Incrementa ax para saber em qual nivel do bloco esta
				add	si, ax	

				movsw				; Copia DS:SI para (0a000h:0)
				movsw				; Move word DS:[(E)SI] para ES:[(E)DI]	
				movsw					
				movsw			

			@Fim_do_Desenho:
			inc 	bx			; Passa para o proximo bloco da matriz

			loop @Escreve_Tela_Linha

		pop	bx

		; Parte Responsavel para desenhar os varios niveis do bloco
		add	ax, 8
		cmp	ax, 64
		jne	@Escreve_Tela_Continua1
		xor 	ax, ax
		add	bx, NUMCOL

		@Escreve_Tela_Continua1:

		add	di, 320-((NUMCOL-6)*8)		; Salta para proxima linha
		pop	cx
		loop @Escreve_Tela_Coluna
	
	pop	bx
	pop	cx
	pop	si
	pop	di
	pop	es
	pop	ax

	ret 

Escreve_Tela ENDP

; ---------------------------------------------------------------------------
; FUNCAO AUXILIAR DE ESCREVE TELA
; Escolhe com DS:BX o tipo de bloco a ser desenhado
; Responde com o offset do bloco em SI
; ---------------------------------------------------------------------------

Escolhe_Bloco PROC

			; Define qual bloco a ser desenhado
			@Bloco1:
			cmp	byte ptr ds:[bx],1
			jne	@Bloco2			
				lea	si, Bloco1
				jmp	@Escolhe_Bloco_FIM
			@Bloco2:
			cmp	byte ptr ds:[bx],2
			jne	@Bloco3			
				lea	si, Bloco2
				jmp	@Escolhe_Bloco_FIM
				
			@Bloco3:
			cmp	byte ptr ds:[bx],3
			jne	@Bloco4			
				lea	si, Bloco3
				jmp	@Escolhe_Bloco_FIM

			@Bloco4:
			cmp	byte ptr ds:[bx],4
			jne	@Bloco5			
				lea	si, Bloco4
				jmp	@Escolhe_Bloco_FIM

			@Bloco5:
			cmp	byte ptr ds:[bx],5
			jne	@Bloco6		
				lea	si, Bloco5
				jmp	@Escolhe_Bloco_FIM

			@Bloco6:
			cmp	byte ptr ds:[bx],6
			jne	@Bloco7		
				lea	si, Bloco6
				jmp	@Escolhe_Bloco_FIM

			@Bloco7:
				lea	si, Bloco7

	@Escolhe_Bloco_FIM:

	RET

Escolhe_Bloco ENDP

; ---------------------------------------------------------------------------
; Mostra_Preview
; Nao tem argumentos, nao retorna nada
; ---------------------------------------------------------------------------

Mostra_Preview PROC

	push	es	
	push	ds
	pusha
	
	mov	ax, 0a000h		; Segmento VGA (0a000h)
	mov	es, ax			; Seta ES:DI para vga screen
	lea	bx, PPreview		; Coloca em bx o Offset da peca

	mov	di, POSPreview

	; vai varrer 4 linhas de 8 pixels
	mov	cx, (4)*8
	xor	ax, ax

	@Mostra_Peca_Coluna:

		; Guarda a posicao de em qual linha esta na matriz
		push	cx
		push	bx	
		
		mov	cx, 4

			@Mostra_Peca_Linha:	
			cmp	byte ptr ds:[bx],X

			je	@Mostra_Peca_Nao_Bloco		
				call	Escolhe_Bloco
				jmp	@Mostra_Peca_Desenha
			
			; ----------------------------------------------------------
			; Nao Desenha um Bloco, substitui por background
			; ----------------------------------------------------------
			@Mostra_Peca_Nao_Bloco:
			
				push	ds
				push	si
				
				mov	si, Image_Buffer1
				mov	ds, si
				mov	si, di
				
				movsw	
				movsw	
				movsw	
				movsw	

				pop	si
				pop	ds

				jmp     @Mostra_Peca_Fim
				
			; ----------------------------------------------------------
			; Desenha um Bloco do tipo definido pelo offset de SI
			; ----------------------------------------------------------
			@Mostra_Peca_Desenha:			
					
				; Incrementa ax para saber em qual nivel do bloco esta
				add	si, ax	

				movsw				; Copia DS:SI para (0a000h:0)
				movsw				; Move word DS:[(E)SI] para ES:[(E)DI]	
				movsw					
				movsw			

			@Mostra_Peca_Fim:
			inc 	bx			; Passa para o proximo bloco da matriz

			loop @Mostra_Peca_Linha
		
		pop	bx

		; Parte Responsavel para desenhar os varios niveis do bloco
		add	ax, 8
		cmp	ax, 64
		jne	@Mostra_Peca_Continua1
		xor 	ax, ax
		add	bx, 4

		@Mostra_Peca_Continua1:
		add	di, 320-((4)*8)		; Salta para proxima linha
		pop	cx

		loop @Mostra_Peca_Coluna

	popa
	pop	ds	
	pop	es

	ret

Mostra_Preview ENDP

; ---------------------------------------------------------------------------
; Desenha_Nivel
; Funcao responsavel por desenhar o nivel e a pontuacao do jogador
; Nao Tem Argumentos, Nao retorna nada
; ---------------------------------------------------------------------------

Desenha_Nivel PROC

	push	es	
	push	ds
	pusha
	
	mov	ax, @data
	mov	ds, ax
	mov	ax, 0a000h		; Segmento VGA (0a000h)
	mov	es, ax			; Seta ES:DI para vga screen

	xor	cx, cx
	mov     cl, SScore
	mov	di, POSNivel

	
	; --------------------------------
	; DESENHA O SCORE 
	; --------------------------------	

	cmp	cl, 0			; verifica se tem ALGUM score para desenhar
	je	@Desenha_Nivel_Fim1	

	; Desenha Borda Inferior do Score
	lea	si, Score_Borda
	movsw	
	movsw	
	movsw	
	movsw	
	movsw	
	movsw

	; Desenha o score em si
	sub	di, (320+12)
	@Desenha_Nivel1:
		lea	si, Score_Fill	
		movsw	
		movsw	
		movsw	
		movsw	
		movsw	
		movsw
		sub	di, (320+12)
		loop	@Desenha_Nivel1

	; Desenha Borda Superior do Nivel
	lea	si, Score_Borda
	movsw	
	movsw	
	movsw	
	movsw	
	movsw	
	movsw

	; FIM DO DESENHO DO SCORE
	@Desenha_Nivel_Fim1:


	popa
	pop	ds	
	pop	es

	ret

Desenha_Nivel ENDP

; ---------------------------------------------------------------------------
; Imprime_Fundo		
;		DX <- Offset do Arquivo de Fundo
; ---------------------------------------------------------------------------

Imprime_Fundo PROC
	
	pusha

	; Define Arquivo de fundo	
	lea	si, palete
	mov	ax, Image_Buffer1
	
	call	Espera_Retrace
	call	Mostra_RAW
	call	Escreve_Palete

	mov	si, Image_Buffer1
	mov	di, VGA
	call	Copia_Buffer

	popa
	
	ret

Imprime_Fundo ENDP

; ---------------------------------------------------------------------------
; PASSEI_NIVEL
; Verifica se passou de nivel no flag: Change_Level e faz devidas atualizacoes
; Nao retorna nada
; ---------------------------------------------------------------------------
  
PASSEI_NIVEL PROC

	pusha

	cmp	Change_Level, 1		; Verifica se o jogador passou de nivel
	jne	@Passei_Nivel_Fim

	mov	Change_Level, 0		; Marca que ja passou de nivel
	lea	bx, PEstat
	lea	si, PEstatF
	call	COPIA_MATRIZ
	
	call	Escurece_Palete

	mov	dx, Arquivo_Uso
	add	dx, 11
	mov	Arquivo_Uso, dx
	call	Imprime_Fundo

	dec	Limite2		; Aumenta a velocidade do jogo
	inc	SNivel
		
	@Passei_Nivel_Fim:

	popa	

	ret

PASSEI_NIVEL ENDP

; ---------------------------------------------------------------------------
; Mostra_ImagemXY (Esta mostrando com cores invertidas intencionalmente)
; ---------------------------------------------------------------------------
; AX - offset da imagem
; BX - LARGURA
; DX - ALTURA
; DI - Posicao na tela grafica

Mostra_ImagemXY PROC

	pusha
	push	ds
	push	es

	call	Espera_Retrace

	mov	ds, ax
	mov	ax, 0a000h		; Segmento VGA (0a000h)
	mov	es, ax			; Seta ES:DI para vga screen

	mov	ax, 320			; Seta Largura da Tela
	sub	ax, bx			; 320 - Tamanho Imagem = Tamanho para salto de linha	

	xor	si, si

	mov	cx, dx 			; Desce Linhas
	@Mostra_ImagemXY:
		
		push	cx
		mov	cx, bx
		rep	movsb
		add	di, ax
		pop	cx

		loop	@Mostra_ImagemXY

	pop	es
	pop	ds
	popa

	ret

Mostra_ImagemXY ENDP

; ---------------------------------------------------------------------------
; Reconstroi_Tela
; RECONSTROI A TELA DE JOGO
; Nao retorna nada, mas imagem de fundo deve estar em Image_Buffer1
; ---------------------------------------------------------------------------

Reconstroi_Tela PROC

	pusha

	call	Espera_Retrace

	; Recupera Palete
	lea	si, Palete
	call	Escreve_Palete

	; Recupera Fundo
	mov	si, Image_Buffer1
	mov	di, VGA
	call	Copia_Buffer
	
	cmp	Dentro_do_Jogo,0
	JE	@Reconstroi_Tela_Segue
	call	Mostra_Preview
	call	Desenha_Nivel
	@Reconstroi_Tela_Segue:

	popa

	ret
	
Reconstroi_Tela ENDP


; ---------------------------------------------------------------------------
; Help_Jogo - Mostra Help do jogo, nao retorna nada
; ---------------------------------------------------------------------------

Help_Jogo PROC

	pusha

	lea	si, Palete
	lea	di, Palete_Aux
	call	Copia_Palete

	call	Escurece_Palete

	mov	ax, VGA
	lea	si, Palete
	lea	dx, Arq_Help
	call	Mostra_Raw	
	call	Escreve_Palete	

	; Espera o usuario pressionar F1 novamente
	@Help_Jogo_Repete:
	
	MOV	AH, 00h
	INT	16h
	CMP	AH, 3Bh			; Verifica se foi F1 para help	
	JE	@Help_Jogo_Sai

	JMP	@Help_Jogo_Repete
	
	@Help_Jogo_Sai:
	call	Escurece_Palete

	lea	si, Palete_Aux
	lea	di, Palete
	call	Copia_Palete

	call	Reconstroi_Tela

	popa

	ret

Help_Jogo ENDP

; ---------------------------------------------------------------------------
; Pausa_Jogo - Pausa o jogo, nao retorna nada
; ---------------------------------------------------------------------------

MSG_Pausa_Jogo PROC

	pusha

	; Escurece Tela
	call	Escurece_Tela

	; Mostra tela de pausa 
	mov	ax, Image_Buffer2
	lea	si, palete_temp
	lea	dx, Arq_Pausa
	call	Mostra_Raw

	mov	bx, 160
	mov	dx, 50
	mov	di, (320*75)+80
	call	Espera_Retrace
	call	Mostra_ImagemXY


	; Espera o usuario pressionar P novamente
	@Pausa_Jogo_Repete:
	
	MOV	AH, 00h
	INT	16h
	CMP	AH, 19h 	; Verifica se P foi pressionado para pause
	JE	@Pausa_Jogo_Sai

	JMP	@Pausa_Jogo_Repete
	
	@Pausa_Jogo_Sai:

	; Recupera tela antiga
	call	Reconstroi_Tela

	popa

	ret

MSG_Pausa_Jogo ENDP

; ---------------------------------------------------------------------------
; Sair_Jogo
; AH - Retorna tecla pressionada pelo usuario
; ---------------------------------------------------------------------------

MSG_Sair_Jogo PROC

	push	si
	push	di
	push	dx
	push	bx

	; Escurece Tela
	call	Escurece_Tela

	; Mostra tela de pausa 
	mov	ax, Image_Buffer2
	lea	si, palete_temp
	lea	dx, Arq_Sair
	call	Mostra_Raw

	mov	bx, 160
	mov	dx, 50
	mov	di, (320*75)+80
	call	Espera_Retrace
	call	Mostra_ImagemXY

	; Espera o usuario pressionar P novamente
	@Sair_Jogo_Repete:
	
	MOV	AH, 00h
	INT	16h
	CMP	AH, 1Fh 	; Verifica se S foi pressionado 
	JE	@Sair_Jogo_Sai
	CMP	AH, 31h 	; Verifica se N foi pressionado
	JE	@Sair_Jogo_Sai

	JMP	@Sair_Jogo_Repete
	
	@Sair_Jogo_Sai:

	; Recupera tela antiga
	call	Reconstroi_Tela

	pop	bx
	pop	dx
	pop	di
	pop	si

	ret

MSG_Sair_Jogo ENDP

; ---------------------------------------------------------------------------
; Sair_Jogo
; AH - Retorna tecla pressionada pelo usuario
; ---------------------------------------------------------------------------

MSG_Reiniciar_Jogo PROC

	push	si
	push	di
	push	dx
	push	bx

	; Escurece Tela
	call	Escurece_Tela

	; Mostra tela de pausa 
	mov	ax, Image_Buffer2
	lea	si, palete_temp
	lea	dx, Arq_Novo
	call	Mostra_Raw

	mov	bx, 160
	mov	dx, 50
	mov	di, (320*75)+80
	call	Espera_Retrace
	call	Mostra_ImagemXY

	; Espera o usuario pressionar P novamente
	@Reiniciar_Jogo_Repete:
	
	MOV	AH, 00h
	INT	16h
	CMP	AH, 1Fh 	; Verifica se S foi pressionado 
	JE	@Reiniciar_Jogo_Sai
	CMP	AH, 31h 	; Verifica se N foi pressionado
	JE	@Reiniciar_Jogo_Sai

	JMP	@Reiniciar_Jogo_Repete
	
	@Reiniciar_Jogo_Sai:

	; Recupera tela antiga
	call	Reconstroi_Tela

	pop	bx
	pop	dx
	pop	di
	pop	si

	ret

MSG_Reiniciar_Jogo ENDP

; ---------------------------------------------------------------------------
; Entrada_Animacao
; ---------------------------------------------------------------------------

Entrada_Animacao PROC

	pusha

	mov	ax, Image_Buffer2
	lea	si, palete_temp
	lea	dx, Animacao
	call	Mostra_Raw
		
	mov	bx, 130
	mov	dx, 110
	mov	di, (320*30)+95
		
	call	Espera_Retrace
	call	Mostra_ImagemXY
	lea	si, palete_temp
	call	Escreve_Palete

	; Mostra Primeiro Frame da animacao e espera
	mov	cx, 90
	@Entrada_Animacao_Espera2:
	call	Espera_Retrace
	loop	@Entrada_Animacao_Espera2


	; Animacao em si, repete 5 vezes
	mov	cx, 3
	@Entrada_Animacao2:
	push	cx

	mov	cx, 12
	lea	dx, Animacao

		
		@Entrada_Animacao1:

		mov	ax, Image_Buffer2
		lea	si, palete_temp
		call	Mostra_Raw
		
		call	Espera_Retrace
		call	Espera_Retrace

		push	dx
		mov	bx, 130
		mov	dx, 110
		mov	di, (320*30)+95
		
		call	Espera_Retrace
		call	Mostra_ImagemXY
		lea	si, palete_temp
		call	Escreve_Palete

		pop	dx

		add	dx, 10	; Vai para proxima imagem

		loop	@Entrada_Animacao1
	
	pop	cx
	loop	@Entrada_Animacao2

	; Mostra GusFAP Logo e Espera
	call	Espera_Retrace
	call	Espera_Retrace
	mov	ax, VGA
	lea	si, palete
	lea	dx, gusfap
	call	Espera_Retrace
	call	Mostra_Raw	
	call	Escreve_Palete	

	call	Pisca_Palete2
	call	Pisca_Palete2

	mov	cx, 90
	@Entrada_Animacao_Espera:
	call	Espera_Retrace
	loop	@Entrada_Animacao_Espera
	call	Escurece_Palete

	; Mostra FAPArt Logo e Espera
	mov	ax, VGA
	lea	si, palete
	lea	dx, FAPart
	call	Espera_Retrace
	call	Mostra_Raw	
	call	Escreve_Palete	
	
	mov	cx, 120
	@Entrada_Animacao_Espera3:
	call	Espera_Retrace
	loop	@Entrada_Animacao_Espera3

	call	Escurece_Palete

	popa

	ret

Entrada_Animacao ENDP

; ---------------------------------------------------------------------------
; Show_Zerou
; Nao retorna nada
; ---------------------------------------------------------------------------

Show_Zerou PROC

	pusha
	
	call	Pisca_Palete2
	
	; Mostra FAPArt Logo e Espera
	mov	ax, VGA
	lea	si, palete
	lea	dx, Zerou
	call	Espera_Retrace
	call	Mostra_Raw	
	call	Escreve_Palete	
	
	call	Pisca_Palete2

	mov	cx, 600
	@Show_Zerou1:
	call	Espera_Retrace
	loop	@Show_Zerou1

	popa

	ret

Show_Zerou ENDP

; ---------------------------------------------------------------------------
; Show_Game_Over
; Nao retorna nada
; ---------------------------------------------------------------------------

Show_Game_Over PROC

	pusha

	mov	ax, VGA
	lea	si, palete
	lea	dx, Game_Over
	call	Espera_Retrace
	call	Mostra_Raw	
	call	Escreve_Palete	
	mov	ax, Image_Buffer1
	call	Mostra_Raw	

	call	Rank	; Manipula o RANK completamente
	
	mov	ah, 02h
	mov	bh, 0h
	mov	dh, 8
	mov	dl, 15
	int	10h
	mov	bx,dx

	mov	Aux_Rank, 0
	lea	si, Rankdata
	CLD
	LODSB	

; Loop para desenhar o Rank todo
	MOV 	CH, AL
@ShowRank_1:

; Loop para escrever o nome no RANK
	MOV	CL, 3
@ShowRank_2:
	LODSB
	MOV	DL, AL
	MOV	AH, 02h
	INT	21h
	DEC	CL
	JNZ	@ShowRank_2
	
	LEA	dx, String_Vazio
	MOV	ah, 09h
	INT	21h

	LODSB
	call	Show_Game_Over_Aux_NivelShow

	LODSB
	ADD	AL, 30h
	MOV	DL, AL
	MOV	ah, 02h
	INT	21h
	
	MOV	dx, bx
    	MOV	ah, 02h
	MOV	bh, 0h
	ADD	dh, 2
	INT	10h
	MOV	bx, dx

	DEC	CH
	JNZ	@ShowRank_1

	mov	cx, 600
	@Show_Game_Over1:
	call	Espera_Retrace
	loop	@Show_Game_Over1

	popa
	
	ret

Show_Game_Over ENDP

; ---------------------------------------------------------------------------
; Show_Game_Over_Aux_NivelShow
; Nao retorna nada - Funcao AUXILIAR para desenhar barras de score
; ---------------------------------------------------------------------------

Show_Game_Over_Aux_NivelShow PROC

	pusha
	push	es

	xor	cx, cx
	mov	cl, al

	mov	ax, VGA			; Segmento VGA (0a000h)
	mov	es, ax			; Seta ES:DI para vga screen
	
	cmp	cx, 0

	; Verifica se o jogador tem algum ponto
	je	@Show_Game_Over_Aux_NivelShow_FIM 

		xor	bx, bx
		mov	dx, cx
		mov	cx, 8
		@Show_Game_Over_Aux_NivelShow_Loop2:

		mov	di, POSRank
		add	di, Aux_Rank
		add	di, bx
		
		push	cx
		mov	cx, dx
		@Show_Game_Over_Aux_NivelShow_Loop1:
		mov	byte ptr es:[di],7	
		inc	di
		loop	@Show_Game_Over_Aux_NivelShow_Loop1
		pop	cx
		add	bx, 320
		loop	@Show_Game_Over_Aux_NivelShow_Loop2	


	@Show_Game_Over_Aux_NivelShow_FIM :

	add	Aux_Rank, (320*8*2)	; Vai para a proxima linha do rank

	pop	es
	popa

	ret

Show_Game_Over_Aux_NivelShow ENDP

; ---------------------------------------------------------------------------
; Inicia_Variaveis
; Nao retorna nada
; ---------------------------------------------------------------------------

Inicia_Variaveis PROC

	pusha

	; Zera Variaveis
	mov	[SScore], 0
	mov	[SNivel], 1	
        mov	[Limite], Velocidade_Ini
	mov	[Limite2], Velocidade_Ini		
	mov	[Change_Level], 0		

	; Zera Poco
	lea	bx, PEstat
	lea	si, PEstatF
	call	COPIA_MATRIZ

	; Zera Imagem de fundo, coloca a primeira
	lea	dx, Level0
	mov	Arquivo_Uso, dx

	popa

	ret

Inicia_Variaveis ENDP

; ---------------------------------------------------------------------------
; sair_do_Jogo
; Nao retorna nada - Nao empilha nada pq vai sair do jogo
; ---------------------------------------------------------------------------

sair_do_Jogo PROC

	
	; Mostra Nossa Foto
	mov	ax, VGA
	lea	si, palete
	lea	dx, Foto
	call	Espera_Retrace
	call	Mostra_Raw	
	call	Escreve_Palete	
	
	mov	cx, 300
	@sair_do_Jogo_Espera1:
	call	Espera_Retrace
	loop	@sair_do_Jogo_Espera1

	call	Escurece_Palete

	MOV 	AX, 0002h   ; Retorna o modo texto
	INT 	10h         
	MOV 	DX,[IntVec]
	MOV 	DS,[IntVec+2]
	CALL 	SETSET
	MOV 	AH,4Ch      ; Retorna ao DOS
	INT 	21h

	ret

sair_do_Jogo ENDP

; ---------------------------------------------------------------------------
; ===========================================================================
; ---------------------------------------------------------------------------
; Finalmente, FIM.

END MAIN
