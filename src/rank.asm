TITLE Ranking
.MODEL small
.STACK 0100h
.286

PUBLIC	RANK, PutPlayerScr, NewRank, ReadFile, CmpScr, MakePlace

extrn	RankFile:byte, Handle:word, PlayerName:byte, RankData:byte, PosScr:byte, SNivel:byte, SScore:byte
extrn	MsgScrVal:byte, MsgScrInval:byte, MsgNome:byte

.CODE

RANK PROC

	pusha
	push	es
	push	ds	

  	MOV 	AX, @DATA
  	MOV 	DS, AX
  	MOV	ES, AX

  	LEA 	DX, RankFile
  
  	MOV	AH, 3Dh			;abrir arquivo em RankFile
  	MOV	AL, 2
  	INT	21h
  	MOV	Handle, AX
  	JNC	@RANK_FileExist

  	; Arquivo RANK novo 
  	MOV	AH, 3Ch 		;geracao do arq RankFile
  	XOR	CX, CX
 	INT	21h
  	;JC	@RANK_Erro
  	MOV	Handle, AX
  
  	Call 	NewRank
  
  	JMP 	@RANK_GravaDados
  
@RANK_FileExist:
  	Call	ReadFile 
  	JC 	@RANK_Erro
  
  	Call	CmpScr
  	CMP	PosScr, 4
  	JA	@RANK_Invalido
  	JE	@RANK_PutScr
  	MOV	AH, RankData
  	CMP	PosScr, AH
  	JAE	@RANK_PutScr
  	CALL 	MakePlace
@RANK_PutScr:
	mov	ah, 02h
	mov	bh, 0h
	mov	dh, 19
	mov	dl, 6
	int	10h  	

	LEA	dx, MsgScrVal
	MOV	ah, 09h
	INT	21h
	
	mov	ah, 02h
	mov	bh, 0h
	mov	dh, 20
	mov	dl, 6
	int	10h  	
	LEA	dx, MsgNome
	MOV	ah, 09h
	INT	21h
	
	CLD
	MOV	AH, 01h
  	LEA	DI, PlayerName
  	MOV 	CX, 3
@RANK_PutScr1:
  	INT	21h
  	STOSB
  	LOOP @RANK_PutScr1 
	
	LEA	SI, PlayerName
  	LEA 	DI, RankData

	Call 	PutPlayerScr
  
@RANK_GravaDados:
  	MOV  	AH,40h  
  	MOV 	BX, Handle
  	MOV	CX, 26			;bytes a serem gravados
  	LEA	DX, RankData  
  	INT	21h  
	JMP	@RANK_FechaArq	

@RANK_Invalido:
	mov	ah, 02h
	mov	bh, 0h
	mov	dh, 19
	mov	dl, 6
	int	10h  	

	LEA	dx, MsgScrInval
	MOV	ah, 09h
	INT	21h

@RANK_FechaArq:  
  	MOV	AH, 3Eh
  	INT 	21h
  
  JMP   @RANK_Fim

@RANK_Erro:
  MOV 	AH, 02h
  MOV   DL, 'E'
  INT 	21h
  
@RANK_Fim:

	pop	ds
	pop	es
	popa

	ret

RANK ENDP  

;### PutPlayerScr - Insere dados do jogo no buffer
;### DI <- end. dos dados do ranking
;### SI <- end. do PlayerName,SScore,SNivel
PutPlayerScr PROC
  PUSH 	AX
  PUSH  CX

  CLD
  CMP byte ptr [DI], 5
  JE  @PutPlayerScr1
  INC byte ptr [DI]	
@PutPlayerScr1:
  INC DI		;pula o campo de scores presentes

  MOV	AL, 5	
  MUL	[PosScr]	;define a posicao no score
  ADD	DI, AX		;posiciona no lugar correto da memoria

  MOV 	CX, 5
  REP	MOVSB    

  POP CX
  POP AX

  RET
PutPlayerScr ENDP


;### NewRank - Insere Dados na 1a. posicao
NewRank PROC
  PUSH 	AX
  PUSH 	CX
  PUSH 	SI
  PUSH 	DI  
  PUSH	DX

  mov	ah, 02h
  mov	bh, 0h
  mov	dh, 19
  mov	dl, 6
  int	10h  	

  LEA	dx, MsgScrVal
  MOV	ah, 09h
  INT	21h
	
  mov	ah, 02h
  mov	bh, 0h
  mov	dh, 20
  mov	dl, 6
  int	10h  	
  LEA	dx, MsgNome
  MOV	ah, 09h
  INT	21h

  CLD
  MOV	AH, 01h
  LEA	DI, PlayerName
  MOV 	CX, 3
@NewRank_1:
  INT	21h
  STOSB
  LOOP @NewRank_1 

  LEA	SI, PlayerName
  LEA 	DI, RankData
  Call  PutPlayerScr

  POP	DX
  POP 	DI
  POP 	SI
  POP	CX
  POP	AX
  
  RET
NewRank ENDP


;### ReadFile - Le os dados do arquivo
ReadFile PROC
  PUSH 	AX
  PUSH 	CX
  PUSH 	BX
  PUSH 	DX  

  MOV	AH, 3Fh			;ler dados do arquivo
  MOV 	BX, Handle  
  MOV	CX, 26			;bytes a serem lidos  
  LEA	DX, RankData  
  INT	21h
  JC	@ReadFile_Erro

  MOV	AH, 42h			;ajusta o ponteiro p inicio
  MOV 	AL, 0
  XOR 	CX, CX
  XOR 	DX, DX
  INT	21h

@ReadFile_Erro:
  POP	DX
  POP	BX
  POP 	CX
  POP 	AX

  RET
ReadFile ENDP


;### CmpScr - compara score do jogador com aqueles do ranking.
;### define se o score e valido GoodScr->1
;### define a posicao de entrada do Score, PosScr
CmpScr PROC
  PUSH 	SI
  PUSH 	AX
  PUSH  CX
	
  MOV 	PosScr,	5

  LEA	SI, RankData
  MOV 	AL, 5 
  MUL	PosScr


  ADD	SI, AX			;posiciona em cima do SNivel
   
   
  STD
@CmpScr1:
  PUSH 	SI  
  LODSB				;carrega o valor para comp SNivel
  CMP 	AL, SNivel
  JA	@CmpScr_Fim1
  JB	@CmpScr_Valido
  LODSB			
  CMP	AL, SScore
  JAE	@CmpScr_Fim1

@CmpScr_Valido:
  POP	SI
  CMP	PosScr, 0
  JE	@CmpScr_Fim2
  DEC	PosScr
  SUB	SI, 5
  JMP   @CmpScr1

@CmpScr_Fim1:
  POP 	SI
  
@CmpScr_Fim2:
  POP	CX
  POP	AX
  POP	SI

  RET
CmpScr ENDP


;### MakePlace - abre espaco para o score
MakePlace PROC

  PUSH 	AX
  PUSH	SI
  PUSH	DI
  PUSH	CX  

  MOV 	AL,5
  MUL	PosScr

  STD
  LEA	SI, RankData
  MOV	DI, SI
  ADD	SI, 20
  ADD	DI, 25
  MOV 	CX, 20
  SUB	CX, AX
  REP 	MOVSB
  
  POP	CX
  POP	DI
  POP	SI
  POP	AX

  RET

MakePlace ENDP
END